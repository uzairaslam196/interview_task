defmodule InterviewTask.Order do
  require InterviewTask.CommonField
  use Ecto.Schema

  import Ecto.Changeset

  use InterviewTask.PK

  schema "orders" do
    field(:quantity, :integer)

    belongs_to(:product_warehouse, InterviewTask.ProductWarehouse,
      foreign_key: :product_warehouse_id,
      references: :id
    )

    belongs_to(:user, InterviewTask.User, foreign_key: :user_id, references: :id)
    field(:shipping_address, :string)
    field(:is_completed, :boolean, default: false)
    field(:deleted_at, :utc_datetime)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [
      :quantity,
      :user_id,
      :product_warehouse_id,
      :shipping_address,
      :is_completed,
      :deleted_at
    ])
    |> validate_required([
      :user_id,
      :quantity,
      :product_warehouse_id,
      :shipping_address
    ])
  end
end
