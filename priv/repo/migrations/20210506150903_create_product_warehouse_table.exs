defmodule Data.Repo.Migrations.CreateProductWarehouseTable do
  use Ecto.Migration

  def change do
    create table(:product_warehouses, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:quantity, :integer)
      add(:product_id, references(:products, column: :id, type: :uuid))
      add(:warehouse_id, references(:warehouses, column: :id, type: :uuid))

      add(:inserted_by_id, references(:users, column: :id, type: :uuid))
      add(:updated_by_id, references(:users, column: :id, type: :uuid))
      add(:deleted_by_id, references(:users, column: :id, type: :uuid))

      timestamps()
    end
    create unique_index(:product_warehouses, [:product_id, :warehouse_id])
  end
end
