defmodule Data.Repo.Migrations.CreateProductTable do
  use Ecto.Migration

  def change do
    create table(:products, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:title, :string, null: false)
      add(:concise_name, :string, null: false)
      add(:deleted_at, :utc_datetime)

      add(:inserted_by_id, references(:users, column: :id, type: :uuid))
      add(:updated_by_id, references(:users, column: :id, type: :uuid))
      add(:deleted_by_id, references(:users, column: :id, type: :uuid))

      timestamps()
    end
    create unique_index(:products, [:concise_name])
  end
end
