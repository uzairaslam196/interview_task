defmodule InterviewTaskWeb.OrderController do
  use InterviewTaskWeb, :controller
  alias InterviewTask.Order
  alias InterviewTask.User
  alias InterviewTask.Warehouse
  alias InterviewTask.ProductWarehouse
  alias InterviewTask.Product

  def create(_conn, params) do
    changeset = Order.changeset(%Order{}, params)

    with {:ok, _rec} <- Repo.insert(changeset) do
      # here i will render to page
      # render(conn, record)
    end
  end

  # to fetch the orders on today
  def get_orders_for_today() do
    query = get_order_query()
    from(q in query,
      where:
        q.inserted_at <= ^NaiveDateTime.new(Date.utc_today(), ~T[00:00:00]) and
          q.is_completed == false
    )
    |> Repo.all()
  end

  # to fetch the orders for next day
  def get_orders_for_next_day() do
    query = get_order_query()
    from(q in query,
      where:
        q.inserted_at > ^NaiveDateTime.new(Date.utc_today(), ~T[00:00:00]) and
          q.is_completed == false
    )
    |> Repo.all()
  end


  #  query to get all orders
  def get_order_query() do
    from(q in Order,
      join: pw in ProductWarehouse,
      on: q.product_warehouse_id == pw.id,
      join: wh in Warehouse,
      on: pw.warehouse_id == wh.id,
      join: p in Product,
      on: p.id == pw.product_id,
      join: usr in User,
      on: q.user_id == usr.id,
      select: %{
        phone_number: usr.phone,
        shipping_address: q.shipping_address,
        product_name: p.title,
        product_id: p.id,
        quanity: q.quantity,
        address: wh.address,
        warehouse_name: wh.title,
        warehouse_number: wh.number,
        user_name: usr.user_name
      })
  end

  #  after completed bulk order we will set is_completed = true for completed orders so that they
  # cannot loop in again
end
