defmodule Data.Repo.Migrations.CreateOrderTable do
  use Ecto.Migration

  def change do
    create table(:orders, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:quantity, :integer)
      add(:shipping_address, :string)
      add(:is_completed, :boolean, default: false)
      add(:product_warehouse_id, references(:product_warehouses, column: :id, type: :uuid))
      add(:user_id, references(:users, column: :id, type: :uuid))

      add(:inserted_by_id, references(:users, column: :id, type: :uuid))
      add(:updated_by_id, references(:users, column: :id, type: :uuid))
      add(:deleted_by_id, references(:users, column: :id, type: :uuid))

      timestamps()
    end
  end
end
