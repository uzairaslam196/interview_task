defmodule InterviewTaskWeb.UserController do
  use InterviewTaskWeb, :controller

  def create(conn, params) do
    params =
      if Map.has_key?(params, "password") do
        Map.merge(params, %{
          "hash_password" => Bcrypt.hash_pwd_salt(params["password"])
        })
      else
        params
      end

    changeset = InterviewTask.User.changeset(%InterviewTask.User{}, params)

    changeset =
      if Map.has_key?(params, "password") do
        changeset
        |> Ecto.Changeset.cast(params, [:hash_password])
        |> Ecto.Changeset.validate_required([:hash_password])
        |> Ecto.Changeset.validate_length(:password, min: 8)
      else
        changeset
        |> Ecto.Changeset.validate_required([:password])
      end

    with {:ok, record} <- InterviewTask.Repo.insert(changeset) do
      render(conn, record, status_to_put: :created)
    end
  end
end
