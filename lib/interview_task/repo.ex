defmodule InterviewTask.Repo do
  use Ecto.Repo,
    otp_app: :interview_task,
    adapter: Ecto.Adapters.Postgres
end
