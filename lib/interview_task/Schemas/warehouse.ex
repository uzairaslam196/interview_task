defmodule InterviewTask.Warehouse do
  require InterviewTask.CommonField
  use Ecto.Schema

  import Ecto.Changeset

  use InterviewTask.PK

  schema "warehouses" do
    field(:title, :string)
    field(:number, :integer)
    field(:address, :string)

    field(:deleted_at, :utc_datetime)

    has_many(:product_warehouses, InterviewTask.ProductWarehouse, foreign_key: :warehouse_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [
      :address,
      :title,
      :number,
      :deleted_at
    ])
    |> validate_required([
      :address,
      :title,
      :concise_name
    ])
  end
end
