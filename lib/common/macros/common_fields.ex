defmodule InterviewTask.CommonField do
  defmacro relational_fields(schema) do
    quote do
      belongs_to(:inserted_by, unquote(schema), foreign_key: :inserted_by_id, references: :id)
      belongs_to(:updated_by, unquote(schema), foreign_key: :updated_by_id, references: :id)
      belongs_to(:deleted_by, unquote(schema), foreign_key: :deleted_by_id, references: :id)
    end
  end
end
