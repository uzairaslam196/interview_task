defmodule InterviewTask.Product do
  require InterviewTask.CommonField
  use Ecto.Schema

  import Ecto.Changeset

  use InterviewTask.PK

  schema "products" do
    field(:title, :string)
    field(:concise_name, :string)

    field(:deleted_at, :utc_datetime)

    has_many(:product_warehouses, InterviewTask.ProductWarehouse, foreign_key: :product_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [
      :title,
      :concise_name,
      :deleted_at
    ])
    |> validate_required([
      :title,
      :concise_name
    ])
  end
end
