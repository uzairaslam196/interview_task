defmodule Data.Repo.Migrations.CreateUserTable do
  use Ecto.Migration

  def change do
    execute("CREATE TYPE user_types AS ENUM ('company', 'individual')")

    create table(:users, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:email, :string, null: false)
      add(:phone, :string, null: false)
      add(:user_name, :string, null: false)
      add(:user_type, :user_types)
      add(:hash_password, :string)
      add(:deleted_at, :utc_datetime)

      add(:inserted_by_id, references(:users, column: :id, type: :uuid))
      add(:updated_by_id, references(:users, column: :id, type: :uuid))
      add(:deleted_by_id, references(:users, column: :id, type: :uuid))

      timestamps()
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:user_name])
  end
end
