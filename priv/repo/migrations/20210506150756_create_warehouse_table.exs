defmodule Data.Repo.Migrations.CreateWareHouseTable do
  use Ecto.Migration

  def change do
    create table(:warehouses, primary_key: false) do
      add(:id, :uuid, primary_key: true)
      add(:title, :string, null: false)
      add(:address, :string, null: false)
      add(:number, :integer)
      add(:deleted_at, :utc_datetime)

      add(:inserted_by_id, references(:users, column: :id, type: :uuid))
      add(:updated_by_id, references(:users, column: :id, type: :uuid))
      add(:deleted_by_id, references(:users, column: :id, type: :uuid))

      timestamps()
    end
    create unique_index(:warehouses, [:number])
  end
end
