defmodule InterviewTask.User do
  require InterviewTask.CommonField
  use Ecto.Schema

  import Ecto.Changeset

  use InterviewTask.PK

  schema "users" do
    field(:email, :string)
    field(:phone, :string)
    field(:hash_password, :string)
    field(:user_name, :string)
    field(:user_type, :string)

    field(:password, :string, virtual: true)
    field(:deleted_at, :utc_datetime)

    has_many(:orders, InterviewTask.Order, foreign_key: :user_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [
      :email,
      :phone,
      :hash_password,
      :user_name,
      :user_type,
      :deleted_at,
      :password
    ])
    |> validate_required([
      :email,
      :user_name,
      :password,
      :user_type
    ])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> unique_constraint(:user_name)
  end
end
