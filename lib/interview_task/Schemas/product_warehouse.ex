defmodule InterviewTask.ProductWarehouse do
  require InterviewTask.CommonField
  use Ecto.Schema

  import Ecto.Changeset

  use InterviewTask.PK

  schema "product_warehouses" do
    belongs_to(:product, InterviewTask.Product, foreign_key: :product_id, references: :id)
    belongs_to(:warehouse, InterviewTask.Warehouse, foreign_key: :warehouse_id, references: :id)
    field(:quantity, :integer)
    field(:deleted_at, :utc_datetime)

    has_many(:orders, InterviewTask.Order, foreign_key: :product_warehouse_id)

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [
      :product_id,
      :warehouse,
      :deleted_at,
      :quantity
    ])
    |> validate_required([
      :product_id,
      :warehouse,
      :quantity
    ])
  end
end
